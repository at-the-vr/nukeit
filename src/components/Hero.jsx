export default function Hero() {
  return (
    <div className="mx-auto flex min-h-screen w-full max-w-4xl flex-col items-center justify-center gap-10 px-4 text-center xl:max-w-6xl">
      <div className="flex flex-col gap-4">
        <p className="text-2xl uppercase">its time to get</p>
        <h1 className="md:7xl text-4xl font-semibold uppercase sm:text-6xl lg:text-8xl">
          swole<span className="text-blue-400">normous</span>
        </h1>
      </div>
      <p className="text-base font-light md:text-lg">
        I hereby acknowledgement that I may become{" "}
        <span className="font-medium text-blue-400">
          unbelievably swolenormous
        </span>{" "}
        and accept all risks of becoming the local{" "}
        <span className="font-medium text-blue-400">mass montrosity</span>,
        afflicted with severe body dismorphia, unable to fit through doors.
      </p>
      <button className="blue-shadow rounded-md border border-solid border-blue-400 bg-slate-950 px-8 py-4 duration-200">
        Accept & begin
      </button>
    </div>
  );
}
